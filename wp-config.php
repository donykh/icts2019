<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'IF_ICTS_2017');

/** MySQL database username */
define('DB_USER', 'icts2017');

/** MySQL database password */
define('DB_PASSWORD', '1ctsy3ah');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L4#zof.5IS%%~wR}.U?E1z8U9R@p[yGG>h_GuU{oUeyCf}WJ-PihLU4O:d$SbUg&');
define('SECURE_AUTH_KEY',  'mK[5qr|;/!rL/;& 3jtu6KpKi`f1BRhcmpgTU(<CWWR0Z;,[TnQkglVER_C9NfO0');
define('LOGGED_IN_KEY',    'd_]?t;3_:{g-6F)18mAs]qk(LQ9XCD<(D5,W)y~854UUeoGi&f?%UmuSSyASm[*D');
define('NONCE_KEY',        '+ldI8vNbz:DB~zEe!:p%XnBCX.7Bj#hd38vl-ZB6ckA7N0aoj69gEG9sPR_W791w');
define('AUTH_SALT',        'xblQ99}DF?l0]O ik!;dGLE#Z)|7<Z2&dmBqy`3Qm{KfP^_tL:IH$)J^Xx4}IAcZ');
define('SECURE_AUTH_SALT', 'dj<W5],4z+k921wQXJ=.M|tS@I%aQzzPppGg}be<~rHY%IQCSl:e,wSTx7Nm`DD?');
define('LOGGED_IN_SALT',   '@H{e-m>h[l:l4xQ4^_vd)qeRd-ejdN#RFe`wnC:a0vt=cxs<(a;y+CQW}Y)1=A<}');
define('NONCE_SALT',       '!Sh6?cKC;Mtl.+4i>tFZfYYPXygTRN;Suo.= HBY)c_,+ +D/?C,e=i!/q>0@}_Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
