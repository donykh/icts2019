<?php
/*
Plugin Name: Mage Event Framework
Description: Mage Event Framework for Event theme by Mage Themes
Version: 1.0.0
Author: Mage Themes
Author URI: http://mage-themes.com
*/

class Mage_Event_Framework {

  function __construct() {
    add_action( 'init', array( $this, 'cpt_speakers' ) );
    add_action( 'init', array( $this, 'cpt_topics' ) );
    add_action( 'init', array( $this, 'cpt_locations' ) );
    add_action( 'init', array( $this, 'cpt_events' ) );
    add_action( 'init', array( $this, 'cpt_tickets' ) );
    add_action( 'init', array( $this, 'cpt_partners' ) );
  }

  // Speakers CPT
  function cpt_speakers() {
    $labels = array(
      'name' => 'Speakers',
      'singular_name' => 'Speaker',
    );

    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'page-attributes'),
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 4,
      'menu_icon' => 'dashicons-businessman',
      'show_in_nav_menus' => false,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => true,
      'capability_type' => 'page',
    );

    register_post_type('speakers', $args);
  }

  // Topics CPT
  function cpt_topics() {
    $labels = array(
      'name' => 'Topics',
      'singular_name' => 'Topic',
    );

    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'page-attributes'),
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 5,
      'menu_icon' => 'dashicons-book',
      'show_in_nav_menus' => false,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => true,
      'capability_type' => 'page',
      'rewrite' => array('slug' => 'topics'),
    );

    register_post_type('Topics', $args);
  }

  // Tickets CPT
  function cpt_tickets() {
    $labels = array(
      'name' => 'Tickets',
      'singular_name' => 'Ticket',
    );

    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'page-attributes'),
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 6,
      'menu_icon' => 'dashicons-tickets-alt',
      'show_in_nav_menus' => false,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => true,
      'capability_type' => 'page',
    );

    register_post_type('Tickets', $args);
  }

  // Locations CPT
  function cpt_locations() {
    $labels = array(
      'name' => 'Locations',
      'singular_name' => 'Location',
    );

    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'page-attributes'),
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 7,
      'menu_icon' => 'dashicons-location',
      'show_in_nav_menus' => false,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => true,
      'capability_type' => 'page',
    );

    register_post_type('Locations', $args);
  }

  // Partners CPT
  function cpt_partners() {
    $labels = array(
      'name' => 'Partners',
      'singular_name' => 'Partner',
    );

    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'page-attributes'),
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 8,
      'menu_icon' => 'dashicons-admin-users',
      'show_in_nav_menus' => false,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => true,
      'capability_type' => 'page',
    );

    register_post_type('Partners', $args);
  }

  // Events CPT
  function cpt_events() {
    $labels = array(
      'name' => 'Events',
      'singular_name' => 'Event',
    );

    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'page-attributes'),
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_position' => 9,
      'menu_icon' => 'dashicons-clock',
      'show_in_nav_menus' => false,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => true,
      'capability_type' => 'page',
    );

    register_post_type('Events', $args);
  }
  
}

$Mage_Event_Framework = new Mage_Event_Framework();