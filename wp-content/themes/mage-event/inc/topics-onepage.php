<a id="topics" class="anchor"></a>



<!-- topics -->

<div class="event-topics">
  <div class="container">

    <?php
    $upperContentId = get_theme_mod( 'upper_text', customizer_library_get_default('upper_text') );
    $upperContent = get_post( $upperContentId );
    ?>

    <h3><strong><?php echo esc_html( $upperContent->post_title ); ?></strong><br /><?php echo the_field( 'subtitle', $upperContentId ); ?></h3>

    <p><?php echo $upperContent->post_content; ?></p>

    <?php
    $topics_args = array(
      'post_type' => 'topics',
      'orderby' => 'menu_order',
      'meta_key' => 'featured',
      'meta_value' => 'Yes'
    );

    $the_query = new WP_Query( $topics_args );
    ?>

    <?php $x=1; if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <!-- single topic -->
      <div>
        <a id="singletopic<?php echo $x; $x++;?>" class="anchor"></a>
        <i class="fa <?php the_field('icon'); ?> fa-2x"></i>
        <h4><?php the_title(); ?></h4>
        <?php the_content( '...' ); ?>
      </div>
      <!-- single topic ends! -->

    <?php endwhile; endif; ?>
    <?php wp_reset_postdata(); //restore original post data ?>

  </div>
</div>
<!-- topics ends! -->