<a id="committee" class="anchor"></a>

<!-- bullets -->
<div class="bullets">

  <?php
  $topics_args = array(
    'post_type' => 'topics',
    'orderby' => 'menu_order',
    'meta_key' => 'featured',
    'meta_value' => 'No'
  );

  $the_query = new WP_Query( $topics_args );
  ?>

  <?php if ( $the_query->have_posts() ) : ?>

  <h3><?php echo get_theme_mod( 'topics_title', customizer_library_get_default( 'topics_title' ) ); ?></h3>

  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

  <div>
    <h4><i class="fa <?php the_field( 'icon' ); ?>"></i> <?php the_title(); ?></h4>
    <?php the_content('...'); ?>
  </div>

  <?php endwhile; endif; ?>
  <?php wp_reset_postdata(); //restore original post data ?>
</div>
<!-- bullets ends! -->