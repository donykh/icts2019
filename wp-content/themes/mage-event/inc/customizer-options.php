<?php

/**
 * Defines customizer options.
 */
function customizer_library_options() {

  // Theme defaults

  // Stores all the controls that will be added
  $options = array();

  // Stores all the sections to be added
  $sections = array();
  $panels = array();

  // Adds the sections to the $options array
  $options['sections'] = $sections;
  $options['panels'] = $panels;

  // Logo
  $section = 'logo';

  $sections[] = array(
  	'id' => $section,
  	'title' => __( 'Logo', '' ),
  	'priority' => '30',
  	'description' => __( 'Website logo', 'mage-event' )
  );
  $options['logo'] = array(
  	'id' => 'logo',
  	'label'   => __( 'Logo', 'mage-event' ),
  	'section' => $section,
  	'type'    => 'image',
  	'default' => ''
  );

  // Colors
  $section = 'colors';

  $sections[] = array(
      'id' => $section,
      'title' => __('Colors', 'mage-event'),
      'priority' => '80',
  );

  $options['mt_event_d_color'] = array(
      'id' => 'mt_event_d_color',
      'label' => __('Primary Color', 'mage-event'),
      'section' => $section,
      'type' => 'color',
      'default' => '#ea4c0f',
  );

  $panel = 'content';

  $panels[] = array(
      'id' => $panel,
      'title' => __( 'Content', 'mage-event' ),
      'priority' => '100'
  );

  $section = 'event';

  $sections[] = array(
      'id' => $section,
      'title' => __('Event', 'mage-event'),
      'priority' => '80',
      'panel' => $panel
  );

  $options['event_when'] = array(
      'id' => 'event_when',
      'label' => __('When', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '7th to 9th October 2016',
  );

  $options['event_starting_at'] = array(
      'id' => 'event_starting_at',
      'label' => __('Starting at', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Starting at 10am',
  );

  $options['event_city'] = array(
      'id' => 'event_city',
      'label' => __('Location', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'London',
  );

  $options['event_venue'] = array(
      'id' => 'event_venue',
      'label' => __('Venue', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Awesome Venue Name',
  );

  $options['event_tagline'] = array(
      'id' => 'event_tagline',
      'label' => __('Tagline', 'mage-event'),
      'section' => $section,
      'type' => 'textarea',
      'default' => 'Tickets are on sale now, get 10% off till November 1st!',
  );

  $options['register_now'] = array(
      'id' => 'register_now',
      'label' => __('Register Now Button', 'mage-event'),
      'description' => __('Register Now Button Text', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Register Now',
  );

  $section = 'parallax';

  $sections[] = array(
      'id' => $section,
      'title' => __('Parallax', 'mage-event'),
      'priority' => '80',
      'panel' => $panel
  );

  $options['parallax_one'] = array(
      'id' => 'parallax_one',
      'label'   => __( 'Header parallax image', 'mage-event' ),
      'section' => $section,
      'type'    => 'upload',
      'default' => '',
  );

  $options['parallax_two'] = array(
      'id' => 'parallax_two',
      'label'   => __( 'Testimonials parallax image', 'mage-event' ),
      'section' => $section,
      'type'    => 'upload',
      'default' => '',
  );

  $options['parallax_three'] = array(
      'id' => 'parallax_three',
      'label'   => __( 'Location and Venue parallax image', 'mage-event' ),
      'section' => $section,
      'type'    => 'upload',
      'default' => '',
  );

  $section = 'general-content';

  $sections[] = array(
      'id' => $section,
      'title' => __('General content', 'mage-event'),
      'priority' => '80',
      'description' => 'Select Pages for content in specific sections.',
      'panel' => $panel
  );

  $options['upper_text'] = array(
      'id' => 'upper_text',
      'label' => __( 'Event intro', 'mage-event' ),
      'section' => $section,
      'type' => 'dropdown-pages',
      'default' => ''
  );

  $options['speakers_text'] = array(
      'id' => 'speakers_text',
      'label' => __( 'Speakers', 'mage-event' ),
      'section' => $section,
      'type' => 'dropdown-pages',
      'default' => ''
  );

  $options['conference_text'] = array(
      'id' => 'conference_text',
      'label' => __( 'Conference Schedule', 'mage-event' ),
      'section' => $section,
      'type' => 'dropdown-pages',
      'default' => ''
  );

  $options['testimonials_title'] = array(
      'id' => 'testimonials_title',
      'label' => __('Testimonials title', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Testimonials',
  );

  $options['topics_title'] = array(
      'id' => 'topics_title',
      'label' => __('Topics title', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Why is this conference for you?',
  );

  $options['registration_text'] = array(
      'id' => 'registration_text',
      'label' => __( 'Pricing and Registration', 'mage-event' ),
      'section' => $section,
      'type' => 'dropdown-pages',
      'default' => ''
  );

  $options['sponsors_text'] = array(
      'id' => 'sponsors_text',
      'label' => __( 'Sponsors, Partners', 'mage-event' ),
      'section' => $section,
      'type' => 'dropdown-pages',
      'default' => ''
  );

  $options['sponsors_button'] = array(
      'id' => 'sponsors_button',
      'label' => __('Get in Touch', 'mage-event'),
      'description' => __('Change Get in Touch link text', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Get in touch',
  );

  $options['location_text'] = array(
      'id' => 'location_text',
      'label' => __( 'Location and Venue', 'mage-event' ),
      'section' => $section,
      'type' => 'dropdown-pages',
      'default' => ''
  );

  $options['location_additional_details'] = array(
      'id' => 'location_additional_details',
      'label' => __('Additional details', 'mage-event'),
      'section' => $section,
      'type' => 'textarea',
      'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  );

  $options['location_image'] = array(
      'id' => 'location_image',
      'label'   => __( 'Location image', 'mage-event' ),
      'section' => $section,
      'type'    => 'upload',
      'default' => '',
  );


  $section = 'map';

  $sections[] = array(
      'id' => $section,
      'title' => __('Google Map', 'mage-event'),
      'priority' => '80',
  );

  $options['map_lat'] = array(
      'id' => 'map_lat',
      'label' => __('Map Latitude', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '-37.817314',
  );

  $options['map_lng'] = array(
      'id' => 'map_lng',
      'label' => __('Map Longitude', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '144.955431',
  );

  $options['map_api_key'] = array(
      'id' => 'map_api_key',
      'label' => __('Google API Key', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['map_zoom'] = array(
      'id' => 'map_zoom',
      'label' => __('Map Zoom Level', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '16',
  );
  $choices = array(
      '1' => 'Yes',
      '0' => 'No',
  );

  $options['map_bw'] = array(
      'id' => 'map_bw',
      'label'   => __( 'Map Black & White mod', 'mage-event' ),
      'section' => $section,
      'type'    => 'radio',
      'choices' => $choices,
      'default' => '1'
  );

  $choices = array(
      '1' => 'Yes',
      '0' => 'No',
  );

  $options['map_scrollwhell'] = array(
      'id' => 'map_scrollwhell',
      'label'   => __( 'Map Disable Zoom on Scroll', 'mage-event' ),
      'section' => $section,
      'type'    => 'radio',
      'choices' => $choices,
      'default' => '1'
  );


  $section = 'footer';

  $sections[] = array(
      'id' => $section,
      'title' => __('Footer', 'mage-event'),
      'priority' => '80',
  );

  $options['footer_title'] = array(
      'id' => 'footer_title',
      'label' => __('Title', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Keep me informed',
  );

  $options['footer_subtitle'] = array(
      'id' => 'footer_subtitle',
      'label' => __('Subtitle', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => 'Signup to our newsletter or connect via social networks',
  );

  $options['footer_copyright'] = array(
      'id' => 'footer_copyright',
      'label' => __('Copyright notice', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['newsletter_shortcode'] = array(
      'id' => 'newsletter_shortcode',
      'label' => __('Newsletter shortcode', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['facebook_link'] = array(
      'id' => 'facebook_link',
      'label' => __('Facebook profile URL', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['twitter_link'] = array(
      'id' => 'twitter_link',
      'label' => __('Twitter profile URL', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['instagram_link'] = array(
      'id' => 'instagram_link',
      'label' => __('Instagram profile URL', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['linkedin_link'] = array(
      'id' => 'linkedin_link',
      'label' => __('Linkedin profile URL', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['google_plus_link'] = array(
      'id' => 'google_plus_link',
      'label' => __('Google+ profile URL', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );


  $section = 'contact-form';

  $sections[] = array(
      'id' => $section,
      'title' => __('Contact forms', 'mage-event'),
      'description' => __('Contact forms shortcodes', 'mage-event'),
      'priority' => '80',
  );

  $options['form_shortcode'] = array(
      'id' => 'form_shortcode',
      'label' => __('Registration form', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  $options['popup_form_shortcode'] = array(
      'id' => 'popup_form_shortcode',
      'label' => __('Contact form', 'mage-event'),
      'section' => $section,
      'type' => 'text',
      'default' => '',
  );

  // Adds the sections to the $options array
  $options['sections'] = $sections;
  $options['panels'] = $panels;

  $customizer_library = Customizer_Library::Instance();
  $customizer_library->add_options($options);

  // To delete custom mods use: customizer_library_remove_theme_mods();
}

add_action('init', 'customizer_library_options');