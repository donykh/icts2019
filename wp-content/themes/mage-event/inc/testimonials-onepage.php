<!-- testimonials -->
<div class="testimonials">

  <h4><?php /*echo get_theme_mod( 'testimonials_title', customizer_library_get_default( 'testimonials_title' ) );*/ echo "Our Previous Events"; ?></h4>

  <?php
  $testimonials_args = array(
    'post_type' => 'post',
    'orderby' => 'menu_order',
    'tax_query' => array(
      array(
      'taxonomy' => 'post_format',
      'field' => 'slug',
      'terms' => 'post-format-quote'
      )
    )
  );

  $the_query = new WP_Query( $testimonials_args );
  ?>

  <div class="slides">
    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

    <blockquote>
      <?php the_content( '...' ); ?>
      <cite><?php the_field( 'name' ); ?><br /><span><?php the_field( 'position' ); ?></span></cite>
    </blockquote>

    <?php endwhile; else: ?>

    <p><?php _e( 'No slides to display here! You will need to add some.', 'mage-event' ); ?></p>

    <?php endif; ?>
    <?php wp_reset_postdata(); //restore original post data ?>
  </div>

</div>
<!-- testimonials ends! -->