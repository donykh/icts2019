<a id="location" class="anchor"></a>

<!-- location -->
<div class="location">
  <div class="container">

    <?php
    $locationContentId = get_theme_mod('location_text', customizer_library_get_default( 'location_text' ));
    $locationContent = get_post( $locationContentId );
    ?>

    <h2><?php echo esc_html( $locationContent->post_title ); ?></h2>
    <p class="subtitle"><?php echo the_field('subtitle', $locationContentId); ?></p>

    <!-- info -->
    <div class="info">
      <div class="maps">

        <?php
        $locationImage = get_theme_mod( 'location_image', customizer_library_get_default( 'location_image' ) ); // attachment ID

        if ( $locationImage != customizer_library_get_default( 'location_image' ) ) {
          echo '<div class="images">';
          echo '<img src="' . esc_url( $locationImage ) . '" >';
          echo '</div>';
        }
        ?>

        <!-- do not remove, needed for Google Map -->
        <div id="map_canvas"></div>
        <!-- do not remove ends! -->

      </div>

      <div class="address">

        <?php if ( $locationContent->post_content ) : ?>
        <h4><?php echo esc_html( $locationContent->post_content ); ?></h4>
        <?php endif; ?>

        <!-- Venue's section -->
        <?php
        $locations_args = array(
          'post_type' => 'locations',
          'orderby' => 'menu_order',
          'meta_key' => 'category',
          'meta_value' => 'Venue'
        );

        $the_query = new WP_Query( $locations_args );
        ?>

        <?php if ( $the_query->have_posts() ) : ?>
        <h5>
          <i class="fa fa-chevron-right"></i> 
          <?php _e( 'Venue', 'mage-event' ); ?>
        </h5>

        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="venue">
          <p><strong><?php the_title(); ?></strong></p>
          <?php the_content(); ?>
          <p><?php the_field( 'full_address' ); ?></p>

          <?php $online_booking_available = get_field( 'online_booking_available' ); ?>

          <?php if ( isset( $online_booking_available[0] ) && $online_booking_available[0] == 'Yes'): ?>
          <a href="<?php the_field('booking_url'); ?>"><?php _e( 'Book Online', 'mage-event' ); ?></a>
          <?php endif; ?>
        </div>

        <?php endwhile; else: ?>

        <!-- <?php _e( 'No venue to display here! You will need to add some.', 'mage-event' ); ?> -->

        <?php endif; ?>

        <?php wp_reset_postdata(); //restore original post data ?>

        <!-- Conference Location section -->
        <?php
        $locations_args = array(
          'post_type' => 'locations',
          'orderby' => 'menu_order',
          'meta_key' => 'category',
          'meta_value' => 'Conference Location'
        );

        $the_query = new WP_Query( $locations_args );
        ?>

        <?php if ( $the_query->have_posts() ) : ?> 
        <h5>
          <i class="fa fa-chevron-right"></i> 
          <?php _e( 'Conference Location', 'mage-event' ); ?>
        </h5>

        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="venue">

          <p><strong><?php the_title(); ?></strong></p>
          <?php the_content(); ?>
          <p><?php the_field( 'full_address' ); ?></p>

          <?php $online_booking_available = get_field( 'online_booking_available' ); ?>

          <?php if ( isset( $online_booking_available[0] ) && $online_booking_available[0] == 'Yes'): ?>
          <a href="<?php the_field( 'booking_url' ); ?>"><?php _e( 'Book Online', 'mage-event' ); ?></a>
          <?php endif; ?>

        </div>

        <?php endwhile; else: ?>

        <!-- <?php _e( 'No venue to display here! You will need to add some.', 'mage-event' ); ?> -->

        <?php endif; ?>

        <?php wp_reset_postdata(); //restore original post data ?>

        <!-- Nearby Accomodation section -->
        <?php
        $locations_args = array(
          'post_type' => 'locations',
          'orderby' => 'menu_order',
          'meta_key' => 'category',
          'meta_value' => 'Nearby Accommodation'
        );

        $the_query = new WP_Query( $locations_args );
        ?>

        <?php if ( $the_query->have_posts() ) : ?>

        <h5>
          <i class="fa fa-chevron-right"></i> 
          <?php _e( 'Nearby Accomodation', 'mage-event' ); ?>
        </h5>

        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <div class="venue">

          <p><strong><?php the_title(); ?></strong></p>
          <?php the_content(); ?>
          <p><?php the_field( 'full_address' ); ?></p>

          <?php $online_booking_available = get_field( 'online_booking_available' ); ?>

          <?php if ( $online_booking_available ): ?>
          <a href="<?php the_field( 'booking_url' ); ?>"><?php _e( 'Book Online', 'mage-event' ); ?></a>
          <?php endif; ?>

        </div>

        <?php endwhile; else: ?>

        <!-- <?php _e( 'No venue to display here! You will need to add some.', 'mage-event' ); ?> -->

        <?php endif; ?>

        <!-- Additional details section -->
        <h5>
          <i class="fa fa-chevron-right"></i> 
          <?php _e( 'Additional details', 'mage-event' ); ?>
        </h5>

        <p><?php echo get_theme_mod( 'location_additional_details', customizer_library_get_default( 'location_additional_details' ) ); ?></p>

      </div>

    </div>
    <!-- info ends! -->

  </div>
</div>
<!-- location ends! -->