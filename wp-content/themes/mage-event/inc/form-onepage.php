<!-- contact form overlay popup -->
<div class="overlay" style="display: none;">

  <div class="contact-form">

    <h3><?php _e( 'Contact Us', 'mage-event' ); ?></h3>
    <p class="subtitle"><?php _e( 'We will get back to you as soon as possible', 'mage-event' ); ?></p>

    <!-- contact form -->
    <div class="form">
      <?php echo do_shortcode( get_theme_mod( 'popup_form_shortcode', customizer_library_get_default( 'popup_form_shortcode' ) ) ) ?>
    </div>
    <!-- contact form ends! -->

    <!-- Do Not Remove! -->
    <p class="error"></p>
    <p class="message"></p>
    <!-- Do Not Remove! Ends! -->

    <a href="#" class="close-contact-form"><i class="fa fa-times fa-lg"></i></a>
  </div>

</div>
<!-- contact form overlay popup ends! -->