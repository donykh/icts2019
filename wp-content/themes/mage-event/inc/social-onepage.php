<!-- social -->
<div class="social">
  <div class="container">
    <br /><br /><br />

    <div class="social-div">
      <h2><?php echo get_theme_mod( 'footer_title', customizer_library_get_default( 'footer_title' ) ); ?></h2>
      <p class="subtitle"><?php echo get_theme_mod( 'footer_subtitle', customizer_library_get_default( 'footer_subtitle' ) ); ?></p>

      <?php echo do_shortcode( get_theme_mod( 'newsletter_shortcode', customizer_library_get_default( 'newsletter_shortcode' ) ) ); ?>

      <?php if (get_theme_mod( 'twitter_link', customizer_library_get_default( 'twitter_link' ) ) != customizer_library_get_default( 'twitter_link' ) ): ?>
      <a href="<?php echo get_theme_mod( 'twitter_link', customizer_library_get_default( 'twitter_link' ) ); ?>"><i class="fa fa-twitter"></i></a>
      <?php endif; ?>

      <?php if (get_theme_mod( 'facebook_link', customizer_library_get_default( 'facebook_link' ) ) != customizer_library_get_default( 'facebook_link' ) ): ?>
      <a href="<?php echo get_theme_mod( 'facebook_link', customizer_library_get_default( 'facebook_link' ) ); ?>"><i class="fa fa-facebook"></i></a><br />
      <?php endif; ?>

      <?php if (get_theme_mod( 'instagram_link', customizer_library_get_default( 'instagram_link' ) ) != customizer_library_get_default( 'instagram_link' ) ): ?>
      <a href="<?php echo get_theme_mod( 'instagram_link', customizer_library_get_default( 'instagram_link' ) ); ?>"><i class="fa fa-instagram"></i></a>
      <?php endif; ?>

      <?php if (get_theme_mod( 'google_plus_link', customizer_library_get_default( 'google_plus_link' ) ) != customizer_library_get_default( 'google_plus_link' ) ): ?>
      <a href="<?php echo get_theme_mod( 'google_plus_link', customizer_library_get_default( 'google_plus_link' ) ); ?>"><i class="fa fa-google-plus"></i></a>
      <?php endif; ?>

      <?php if (get_theme_mod( 'linkedin_link', customizer_library_get_default( 'linkedin_link' ) ) != customizer_library_get_default( 'linkedin_link' ) ): ?>
      <a href="<?php echo get_theme_mod( 'linkedin_link', customizer_library_get_default( 'linkedin_link' ) ); ?>"><i class="fa fa-linkedin"></i></a>
      <?php endif; ?></div>


    

  </div>
</div>

<div class="container">
      <br /><a name="contactus"> </a><br />
      <h2>Contact</h2>
<strong>Bagus Jati Santoso, S.Kom., Ph.D. (General Chair)</strong>
<br /><br />
Department of Informatics, Faculty of Information Technology,<br />
Institut Teknologi Sepuluh Nopember (ITS)<br />
Jl. Teknik Kimia, Kampus ITS,<br />
Sukolilo, Surabaya, Indonesia 60111<br /><br />
<strong>Email: icts[at]if.its.ac.id </strong>
    </div>
<!-- social ends! -->