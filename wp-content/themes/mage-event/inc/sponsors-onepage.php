<a id="sponsors" class="anchor"></a>

<!-- sponsors -->
<div class="sponsors">
  <div class="container">

    <?php
    $sponsorsContentId = get_theme_mod( 'sponsors_text', customizer_library_get_default( 'sponsors_text' ) );
    $sponsorsContent = get_post( $sponsorsContentId );
    ?>

    <h2><?php echo esc_html( $sponsorsContent->post_title ); ?></h2>

    <p class="subtitle">
      <?php if ( the_field( 'subtitle', $sponsorsContentId ) ) echo the_field( 'subtitle', $sponsorsContentId ); ?> 
      
      <?php
      $sponsors_button = get_theme_mod('sponsors_button', customizer_library_get_default( 'sponsors_button' ) );
      if ( !empty( $sponsors_button ) ) {
      ?>
      <a href="#" class="open-contact-form"><?php echo $sponsors_button; ?></a>
      <?php }; ?>
    </p>

    <?php if ( $sponsorsContent->post_content ) : ?>
    <p><?php echo $sponsorsContent->post_content; //echo esc_html( $sponsorsContent->post_content ); ?></p>
    <?php endif; ?>

    <?php
    $locations_args = array(
      'post_type' => 'partners',
      'orderby' => 'menu_order'
    );

    $the_query = new WP_Query( $locations_args );
    ?>

    <?php if ( $the_query->have_posts() ) : ?>
    <div class="slides">

      <ul>
      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <li><a href="<?php the_field( 'url' ); ?>" title="<?php the_title(); ?>"><img src="<?php the_field( 'image' ); ?>" height="100" alt="<?php the_title(); ?>" /></a></li>
      <?php endwhile; ?>
      </ul>

    </div>
    <?php else: ?>

    <p><?php _e( 'No partners to display here! You will need to add some.', 'mage-event' ); ?></p>

    <?php endif; ?>
    <?php wp_reset_postdata(); //restore original post data ?>

  </div>
</div>
<!-- sponsors ends! -->