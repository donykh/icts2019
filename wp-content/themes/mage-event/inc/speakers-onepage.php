<a id="speakers" class="anchor"></a>

<!-- speakers -->
<?php
$speakers_args = array(
  'post_type' => 'speakers',
  'orderby' => 'menu_order',
  'meta_key' => 'featured',
  'meta_value' => 'Yes'
);

$the_query = new WP_Query( $speakers_args );
?>

<div class="event-speakers">
  <div class="container">

    <?php
    $speakersContentId = get_theme_mod( 'speakers_text', customizer_library_get_default('speakers_text') );
    $speakersContent = get_post( $speakersContentId );
    ?>

    <h2><?php echo esc_html( $speakersContent->post_title ); ?></h2>
    <p class="subtitle"><?php the_field( 'subtitle', $speakersContentId ); ?></p>
    <p><?php echo esc_html( $speakersContent->post_content ); ?></p>

    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

    <!-- featured -->
    <div class="featured">
    <br />
      <div class="image">
        <img width="200px" src="<?php the_field( 'featured_image' ); ?>" alt="" />
      </div>

      <div>
        <h3><?php the_title(); ?></h4>
        <p class="title"><?php the_field( 'firstname' ); ?> <?php the_field( 'middle_name' ); ?> <?php the_field( 'lastname' ); ?>, <?php the_field( 'position' ); ?></p>
        <?php the_content( '...' ); ?>
      </div>
    <br />
    </div>
    <!-- featured ends! -->
    
    <?php endwhile; else: ?>
      <p class="no-content-display"><?php _e( 'No featured speakers to display here! You will need to add some.', 'mage-event' ); ?></p>
    <?php endif; ?>

    <?php wp_reset_postdata(); //restore original post data ?>

    <?php
    $speakers_args = array(
      'post_type' => 'speakers',
      'orderby' => 'menu_order',
      'meta_key' => 'featured',
      'meta_value' => 'No'
    );

    $the_query = new WP_Query( $speakers_args );
    ?>

    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

      <!-- speaker -->
      <div class="speaker">
        <img src="<?php the_field('profile_image'); ?>" alt="" />

        <div>
          <h3><?php the_field('firstname'); ?><br /><?php the_field('middle_name'); ?> <?php the_field('lastname'); ?></h3>

          <?php if (get_field('position')): ?>
          <p><strong><?php the_field('position'); ?></strong></p>
          <?php endif; ?>

          <p><?php echo get_the_content('...'); ?></p>

          <p>
            <?php if (get_field('twitter_profile')): ?>
            <a href="<?php the_field('twitter_profile'); ?>"><i class="fa fa-twitter fa-lg"></i></a>
            <?php endif; ?>
          </p>

        </div>
      </div>
      <!-- speaker ends! -->

    <?php endwhile; endif; ?>
    <?php wp_reset_postdata(); //restore original post data ?>

  </div>
</div>
<!-- speakers ends! -->