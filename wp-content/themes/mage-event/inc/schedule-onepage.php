<a id="schedule" class="anchor"></a>

<!-- schedule -->
<div class="schedule">
  <div class="container">

    <?php
    $conferenceContentId = get_theme_mod( 'conference_text', customizer_library_get_default( 'conference_text' ) );
    $conferenceContent = get_post( $conferenceContentId );
    ?>

    <h2><?php echo esc_html( $conferenceContent->post_title ); ?></h2>
    <p class="subtitle"><?php echo the_field( 'subtitle', $conferenceContentId ); ?></p>
    <p><?php //echo esc_html( $conferenceContent->post_content ); ?></p>

    
    <p style="text-align: justify; font-size: 14pt">Without compromising the standard, we divide the paper submission into three (3) Phases. The important dates are in the following : <strong><br /><br />Phase I</strong> :<br /> Submission Deadline - April 15th 2017<br /> Notification of Acceptance - <del datetime="2017-05-14T08:34:34+00:00">May 14th 2017</del>  May 17th 2017<br /> Payment Deadline - <del>May 24th 2017</del> May 29th 2017<br />Camera Ready Paper - May 28th 2017<br /><br /> <strong>Phase II</strong> :<br /> Submission Deadline - June 1st 2017<br /> Notification of Acceptance - <del>July 14th 2017</del>  July 21st 2017<br />Payment Deadline - August 18th 2017 <br />Camera Ready Paper - August 20th 2017<br /><br /><strong>Phase III</strong> :<br /> Submission Deadline - August 10th 2017<br /> Notification of Acceptance - <del>September 15th 2017</del> September 19th 2017<br />Payment Deadline - September 27th 2017<br />Camera Ready Paper - September 27th 2017<br /><br /><strong>The Conference Date</strong> : October 31st 2017<br /></p>
<br /><br />
    
    <?php
    $event_args = array(
      'post_type' => 'events',
      'orderby' => 'menu_order'
    );

    $the_query = new WP_Query( $event_args );
    ?>
      
    <div id="schedule-tabs" class="tabs" data-toggle="tabslet" data-animation="true">

    <?php
    if ( $the_query->have_posts() ) {
      $event_days = array();

      while ( $the_query->have_posts() ) {
        $the_query->the_post();
        $get_fields = get_fields();
        $event_days[] = $get_fields['date'];
      }
    }

    if ( !empty( $event_days ) ) { // check if user created any events and sort them!
      $event_days = array_unique( $event_days );
      asort( $event_days );
    }

    if ( !empty( $event_days ) ) { // check if user created any events and create tabs!
      echo '<ul class="idTabs">';

      $i = 1;
      foreach ($event_days as $day) {
        $formatted_day = date('jS', strtotime($day));
        echo '<li><a href="#eventdays-'. $i .'">Day '. $i .', '. $formatted_day . '</a></li>';
        $i++;
      } // end foreach

      echo '</ul>';
    }
    ?>

    <?php
    if ( !empty( $event_days ) ) { // check if user created any events!

    $i = 1;
    foreach ( $event_days as $day ) {
      echo '<div id="eventdays-'. $i.'" class="day">';

      if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

        if ( $day == get_field( 'date' ) ): // rendering single event
    ?>
    <!-- single event -->
    <div class="event <?php if ( get_the_content() ) echo 'extend'; ?>">

      <div class="event-time">
        <?php the_field( 'starts_at' ); if ( get_field( 'ends_at' ) ) echo ' - '. get_field( 'ends_at' ); ?><span><i class="fa fa-angle-up"></i></span>
      </div>

      <div class="event-info">
        <div>
          <h4><?php the_title(); ?></h4>
          <p class="event-speaker">
            <strong><?php the_field( 'speaker' ); ?></strong>
            <br/>
            <?php the_field( 'position' ); ?>
          </p>
          <?php the_content() ?>
        </div>
      </div>

    </div>
    <!-- single event ends! -->

    <?php
      endif; // if ( $the_query->have_posts() )
      endwhile;
      endif; // if ( $day == get_field( 'date' ) )
      $i++;
      echo '</div>';
    }

    } else {
      echo '<p class="no-content-display">No events to display!</p>';
    } // end of 'check if user created any events'
    ?>

    <?php wp_reset_postdata(); //restore original post data ?>

    </div>

  </div>
</div>
<!-- schedule ends! -->