<a id="registration" class="anchor"></a>

<!-- registration -->
<div class="registration">
  <div class="container">

    <?php
    $registrationContentId = get_theme_mod( 'registration_text', customizer_library_get_default( 'registration_text' ) );
    $registrationContent = get_post( $registrationContentId );
    ?>

    <h2><?php echo esc_html( $registrationContent->post_title ); ?></h2>
    <p class="subtitle"><?php echo the_field( 'subtitle', $registrationContentId ); ?></p>
    <p class="desc"><?php echo esc_html( $registrationContent->post_content ); ?></p>

    <h3><?php /*_e( 'Fill out your information', 'mage-event' );*/ _e( 'There are several categories of registration fee', 'mage-event' );?></h3>

    <!-- register form -->
    <div class="form">
    <?php echo do_shortcode( get_theme_mod( 'form_shortcode', customizer_library_get_default( 'form_shortcode' ) ) ) ?>
    </div>
    <!-- register form ends! -->

    <?php
    $tickets_args = array(
      'post_type' => 'tickets',
      'orderby' => 'menu_order'
    );

    $the_query = new WP_Query( $tickets_args );
    ?>

    <!-- prices -->
    <div class="price">

      <?php
	  $count = 0;
	   if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
	   $count++;
	   if($count < 3) {
	   ?>
      
      <div data-ticket-name="<?php the_title(); ?>">
        <p class="amount"><?php the_field( 'price' ); ?></p>

        <h4>
        <?php the_title(); ?>
        <?php
        if ( get_field( 'discounted' ) ) {
          echo '<span>';
          if ( get_field( 'discounted_label' ) ) { echo get_field( 'discounted_label' ); } else { _e( 'Save NOW', 'mage-event' ); }
          echo '</span>';
        }
        ?>
        </h4>

        <?php the_content(); ?>
      </div> 
      <?php }?>
      <?php endwhile;  else: ?>

      <p><?php _e( 'No tickets to display here! You will need to add some.', 'mage-event' ); ?></p>

      <?php endif;  ?>
      <?php wp_reset_postdata(); //restore original post data ?>

    </div>
    
    <div class="price">
       <?php
       $count = 0;
	   if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
	   $count++;
	   if($count > 2) {
	   ?>
      
      <div data-ticket-name="<?php the_title(); ?>">
        <p class="amount"><?php the_field( 'price' ); ?></p>

        <h4>
        <?php the_title(); ?>
        <?php
        if ( get_field( 'discounted' ) ) {
          echo '<span>';
          if ( get_field( 'discounted_label' ) ) { echo get_field( 'discounted_label' ); } else { _e( 'Save NOW', 'mage-event' ); }
          echo '</span>';
        }
        ?>
        </h4>

        <?php the_content(); ?>
      </div> 
      <?php }?>

      <?php endwhile; else: ?>

      <p><?php _e( 'No tickets to display here! You will need to add some.', 'mage-event' ); ?></p>

      <?php endif; ?>
      <?php wp_reset_postdata(); //restore original post data ?>

    </div>
    <!-- prices ends! --><br />&nbsp;<br />
  10% discount is available for IEEE Member.
  <br /><br /><br/>
  <strong>To register and confirm your participation and payment</strong>, please fill online form below :<br />
Registration Form for <a href="https://docs.google.com/forms/d/e/1FAIpQLSeNVvSSwLqRS6V-7Zae9ThyMGAremsOoBrMsODU4-jpBnrA7Q/viewform?usp=sf_link" target="_tab">Author</a> <br />
Registration Form for <a href="https://docs.google.com/forms/d/e/1FAIpQLSdTxuRGCrR1gKT5VQoJi5zeOGNY-wLOW5I0DGebKNZStH7-jA/viewform?usp=sf_link" target="_tab">Participant</a><br />
<br />
  <strong>Banking details for payment </strong>(state clearly the participant’s name to identify your payment): <br /><br />

Name                : DWI SUNARYONO <br/>
Bank                : Bank BNI (Bank Negara Indonesia)<br/>
Account Number      : 0241698838 <br />
SWIFT Code          : BNINIDJA <br />
  </div>
</div>
<!-- registration ends! -->