<?php
/**
 * Implements styles set in the theme customizer.
 */
if ( !function_exists( 'customizer_library_build_styles' ) && class_exists( 'Customizer_Library_Styles' ) ):

  // process user options to generate CSS needed to implement the choices.
  function customizer_library_build_styles() {

    // primary Color
    $setting = 'mt_event_d_color';
    $mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
    $parallaxOne = get_theme_mod( 'parallax_one', customizer_library_get_default( 'parallax_one' ) );
    $parallaxTwo = get_theme_mod( 'parallax_two', customizer_library_get_default( 'parallax_two' ) );
    $parallaxThree = get_theme_mod( 'parallax_three', customizer_library_get_default( 'parallax_three' ) );

    $color = sanitize_hex_color( $mod );

    if ( $parallaxOne != customizer_library_get_default('parallax_one')) {
      Customizer_Library_Styles()->add( array(
        'selectors' => array( '.header', ),
        'declarations' => array( 'background-image' => 'url('.$parallaxOne.')', )
      ) );
    }

    if ($parallaxTwo != customizer_library_get_default( 'parallax_two' ) ) {
      Customizer_Library_Styles()->add( array(
        'selectors' => array('.why',),
        'declarations' => array('background-image' => 'url('.$parallaxTwo.')',)
      ) );
    }

    if ($parallaxThree != customizer_library_get_default( 'parallax_three' ) ) {
      Customizer_Library_Styles()->add( array(
        'selectors' => array('.location',),
        'declarations' => array('background-image' => 'url('.$parallaxThree.')',)
      ) );
    }

    Customizer_Library_Styles()->add( array(
      'selectors' => array('
        .when .icon-holder,
        .where .icon-holder,
        .idTabs li.active,
        .idTabs li.active:hover,
        .event .event-time span,
        .testimonials ul.flex-direction-nav .flex-prev,
        .testimonials ul.flex-direction-nav .flex-next,
        .price div .amount,
        .button,
        .featured,
        .back-to-top a,
        .read-more-continue a,
        .speaker div,
        .form button,
        .form input.wpcf7-submit,
        .social .container,
        .single-page-social,
        .comments-area .submit,
        .mage_event_widget.widget_search input#searchsubmit,
        div.mt-menu,
        ul.children,
        ul.sub-menu
      '),
      'declarations' => array( 'background-color' => $color,), ) );
      
      Customizer_Library_Styles()->add( array(
        'selectors' => array('
          .when strong,
          .where strong,
          .event-topics .container div h4,
          .event-topics .container div .fa,
          .event .event-time,
          .event .event-info h4,
          .event .event-info h4 a,
          .testimonials cite,
          .bullets h4,
          .registration h3,
          .price h4 span,
          .sponsors .subtitle a,
          .venue a,
          .venue strong,
          .social button:hover,
          .social a:hover,
          .social input.button:hover,
          .footer a,
          .contact-form a,
          .comments-area .comment a,
          .mage_event_widget a,
          .address h5 i,
          .form .wpcf7-mail-sent-ok
        ',),
        'declarations' => array( 'color' => $color,), ) );
      
      Customizer_Library_Styles()->add( array(
        'selectors' => array('
          .button,
          .register-now a.button:hover,
          .form .wpcf7-mail-sent-ok,
          .price div.active,
          .price div:hover,
          .form button,
          .form input.wpcf7-submit,
          .mage_event_widget.widget_search input#s,
          .comments-area .submit,
          .mage_event_widget.widget_search input#searchsubmit
        ',),
        'declarations' => array( 'border-color' => $color,), ) );
            
      Customizer_Library_Styles()->add( array(
        'selectors' => array('
          .sponsors .subtitle a:hover,
          .sponsors .subtitle a
        ',),
        'declarations' => array( 'border-bottom-color' => $color,), ) );
            
      Customizer_Library_Styles()->add( array(
        'selectors' => array('
          .mage_event_widget.widget_archive,
          .mage_event_widget.widget_categories,
          .mage_event_widget.widget_meta,
          .mage_event_widget.widget_text,
          .mage_event_widget.widget_recent_comments,
          .mage_event_widget.widget_recent_entries,
          .mage_event_widget.widget_calendar,
          .mage_event_widget.widget_pages,
          .mage_event_widget.widget_rss,
          .mage_event_widget.widget_nav_menu,
          .widget_recent_entries'
          ,),
        'declarations' => array( 'border-left-color' => $color,), ) );
  }

endif;

add_action('customizer_library_styles', 'customizer_library_build_styles');

if ( !function_exists( 'customizer_library_custom_styles' ) ) :

  /**
   * Generates the style tag and CSS needed for the theme options.
   *
   * By using the "Customizer_Library_Styles" filter, different components can print CSS in the header.
   * It is organized this way to ensure there is only one "style" tag.
   */
  function customizer_library_custom_styles() {
    do_action( 'customizer_library_styles' );

    // Echo the rules
    $css = Customizer_Library_Styles()->build();

    if (!empty($css)) {
      echo "\n<!-- Begin Custom CSS -->\n<style type=\"text/css\" id=\"customizer-custom-css\">\n";
      echo $css;
      echo "\n</style>\n<!-- End Custom CSS -->\n";
    }
  }
endif;

add_action( 'wp_head' , 'customizer_library_custom_styles', 11);