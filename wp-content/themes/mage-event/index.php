<?php get_header(); ?>

<!-- Index Page (index.php, which loads blog-post.php) -->
<div class="single-page">
    <div class="container">

      <div class="content-page">

        <?php wp_link_pages(); ?>

        <div class="blog-posts">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'blog', 'post' ); ?>

        <?php endwhile; ?>

        <div class="pagination">
          <div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
          <div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
        </div>

        <?php else : ?>

        <p><?php _e( 'There are no posts or pages here', 'mage-event' ); ?></p>

        <?php endif; ?>
        </div>

      </div>

      <div class="sidebar">

        <div class="widgets">
        <?php if ( is_active_sidebar( 'mage_event_sidebar_widget_id' ) ) { ?>

          <?php dynamic_sidebar( 'mage_event_sidebar_widget_id' ); ?>

        <?php } ?>
        </div>

      </div>

  </div>
</div>
<!-- Single Page Ends! -->

<?php get_footer(); ?>