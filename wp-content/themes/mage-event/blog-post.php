<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

  <?php
  // check if the post has a Post Thumbnail assigned to it.
  if ( has_post_thumbnail() ) {
    the_post_thumbnail();
  }

  ?>

  <div class="excerpt">

    <?php if( is_single() ): ?>

      <ul class="post-meta">
        <li><i class="fa fa-clock-o"></i> <?php the_time( 'jS F Y' ) ?></li>

        <?php if(get_the_category()): ?>
        <li><i class="fa fa-folder-open"></i> <?php the_category( ', ' ) ?></li>
        <?php endif; ?>

        <?php if(get_tags()): ?>
        <li><?php the_tags( '<i class="fa fa-tags"></i>', ', ' ); ?></li>
        <?php endif; ?>

        <li><i class="fa fa-user"></i><?php the_author(); ?></li>
      </ul>

      <?php the_content(); ?>

      <?php comments_template(); ?>

    <?php else: ?>

      <div class="the_excerpt">
        <?php the_excerpt(); ?>
      </div>

      <?php if(get_tags()): ?>
        <p class="tags"><?php the_tags('<i class="fa fa-tags"></i> ', ', '); ?></li>
      <?php endif; ?>

      <p class="read-more-continue"><a class="post-link" href="<?php the_permalink(); ?>">Continue</a></p>

    <?php endif; ?>

  </div>

</div>