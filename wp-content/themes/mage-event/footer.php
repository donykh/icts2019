  <?php if ( is_front_page() ): ?>

  <?php else: ?>

  <div class="single-page-social">
    <div class="container">
      <div class="widgets">
        <?php if ( is_active_sidebar( 'mage_event_footer_widget_id' ) ) { ?>

          <?php dynamic_sidebar( 'mage_event_footer_widget_id' ); ?>

        <?php } ?>
      </div>
    </div>
  </div>

  <?php endif; ?>

  <!-- footer -->
  <div class="footer">
    <div class="container">

      <p>
        <?php if ( get_theme_mod( 'footer_copyright', customizer_library_get_default('footer_copyright')) == '' ) : ?>
        <?php _e( 'Copyright', 'mage-event' ); ?> <?php echo date('Y'); ?>. <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo html_entity_decode(get_bloginfo('name')); ?></a>
        <?php else: ?>
        <?php echo get_theme_mod('footer_copyright', customizer_library_get_default('footer_copyright')); ?> 
        <?php endif; ?>
      </p>
<div style="font-size:4px"><a title="pendik evden eve nakliyat" style="color:#000000" href="http://www.ayazogluevdenevenakliyat.com">pendik evden eve nakliyat</a></div>
    </div>
  </div>
  <!-- footer ends! -->

</div>

<?php wp_footer(); ?>

</body>
</html>
