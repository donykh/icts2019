<?php

if ( !isset( $content_width ) ) { $content_width = 960; }

require_once get_template_directory() . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'mage_event_register_required_plugins' );

function mage_event_register_required_plugins() {
  $plugins = array(
    array(
      'name' => __( 'Advanced Custom Fields', 'mage-event' ),
      'slug' => 'advanced-custom-fields',
      'version' => '4.4.8',
      'required' => true
    ),
    array(
      'name' => __( 'Contact Form 7', 'mage-event' ),
      'slug' => 'contact-form-7',
      'version' => '4.5',
      'required' => true
    ),
    array(
      'name' => __( 'Date and Time Picker Field', 'mage-event' ),
      'slug' => 'acf-field-date-time-picker',
      'version' => '2.1.1',
      'required' => true
    ),
    array(
      'name' => __( 'Mage Event Framework', 'mage-event' ),
      'slug' => 'mage-event-framework',
      'version' => '1.0.0',
      'source' => get_template_directory() . '/lib/plugins/mage-event-framework.zip',
      'required' => true
    ),
    array(
      'name' => __( 'MailChimp for WordPress', 'mage-event' ),
      'slug' => 'mailchimp-for-wp',
      'version' => '4.0.4',
      'required' => true
    )
  );

  $config = array(
    'id' => 'tgmpa',
    'default_path' => '',
    'menu' => 'tgmpa-install-plugins',
    'parent_slug' => 'themes.php',
    'capability' => 'edit_theme_options',
    'has_notices' => true,
    'dismissable' => true,
    'dismiss_msg' => '',
    'is_automatic' => true,
    'message' => '',
  );

  tgmpa( $plugins, $config );
}

add_theme_support( 'menus' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array('quote') );
add_image_size( 'mage_event-speaker', 294, 294, true );
add_image_size( 'mage_event-featured-speaker', 590, 420, true );
add_image_size( 'mage_event-partners', 291, 93, true );

// registering navigation menu
register_nav_menu( 'header-menu', 'Header Menu' );

function mage_event_slug_setup() {
  add_theme_support( 'title-tag' );
}

add_action( 'after_setup_theme', 'mage_event_slug_setup' );

function mage_event_fonts() {
  $fonts_url = '';

  $roboto_condensed = _x( 'on', 'Roboto Condensed font: on or off', 'mage-event' );
  $open_sans = _x( 'on', 'Open Sans font: on or off', 'mage-event' );

  if ( 'off' !== $roboto_condensed || 'off' !== $open_sans ) {
    $font_families = array();
     
    if ( 'off' !== $roboto_condensed ) {
      $font_families[] = 'Roboto Condensed:400,300,700';
    }
     
    if ( 'off' !== $open_sans ) {
      $font_families[] = 'Open Sans:400,600,700,800,';
    }
  }

  $query_args = array(
    'family' => urlencode( implode( '|', $font_families ) ),
    'subset' => urlencode( 'latin,latin-ext' ),
  );

  $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
   
  return $fonts_url;
}

// create widgets
function mage_event_create_widget( $name, $id, $description ) {
  $args = array(
    'name' => $name,
    'id' => $id,
    'description' => $description,
    'before_widget' =>
    '<div class="mage_event_widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>'
  );

  register_sidebar( $args );
}

function mage_event_add_widgets() {
  mage_event_create_widget( __( 'Sidebar widgets', 'mage-event' ), 'mage_event_sidebar_widget_id', __( 'Widget area in sidebar the page for blog posts', 'mage-event' ) );
  mage_event_create_widget( __( 'Footer widget', 'mage-event' ), 'mage_event_footer_widget_id', __( 'Widget area in footer of the page for blog posts', 'mage-event' ) );
}

add_action( 'widgets_init', 'mage_event_add_widgets' );

// load the theme CSS
function mage_event_styles() {
  wp_enqueue_style( 'mage_event_main', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'font_awesome', get_template_directory_uri() . '/scripts/font-awesome-4.6.3/css/font-awesome.min.css' );
  wp_enqueue_style( 'animate', get_template_directory_uri() . '/animate.min.css' );
  wp_enqueue_style( 'mage-theme-fonts', mage_event_fonts(), array(), null );
}

add_action( 'wp_enqueue_scripts', 'mage_event_styles' );

// load the theme JS
function mage_event_scripts() {

  if ( ( is_page_template( array ( 'onepage_var1.php', 'onepage.php' ) /*is_home() || is_front_page()*/ ) ) ) {
    wp_enqueue_script( 'mage_event_woothemes_flexslider', get_template_directory_uri() . '/scripts/woothemes-flexslider/jquery.flexslider-min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'mage_event_google_map', '//maps.googleapis.com/maps/api/js?key=' . esc_html( get_theme_mod( 'map_api_key', customizer_library_get_default( 'map_api_key' ) ) ) . '&callback=initMap', array(), '', true );
    wp_enqueue_script( 'mage_event_flexslider', get_template_directory_uri() . '/scripts/flexslider.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'jquery.tabslet', get_template_directory_uri() . '/scripts/jquery.tabslet.min.js', array( 'jquery' ), '', true );
  }

  wp_enqueue_script( 'jquery.inview', get_template_directory_uri() . '/scripts/jquery.inview.min.js', array( 'jquery' ), '', true );
  wp_enqueue_script( 'jquery.parallax', get_template_directory_uri() . '/scripts/jquery.parallax-1.1.3.js', array( 'jquery' ), '', true );
  wp_enqueue_script( 'mage_event_theme_js', get_template_directory_uri() . '/scripts/theme.js', array( 'jquery', 'jquery-ui-tabs' ), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'mage_event_scripts' );

// Google maps initalize initMap function
function mage_event_gmap() {

  $map_lat = get_theme_mod( 'map_lat', customizer_library_get_default( 'map_lat' ) );
  $map_lng = get_theme_mod( 'map_lng', customizer_library_get_default( 'map_lng' ) );
  $map_zoom = get_theme_mod( 'map_zoom', customizer_library_get_default( 'map_zoom' ) );

  if ( get_theme_mod( 'map_scrollwhell', customizer_library_get_default( 'map_scrollwhell' ) ) ) { 
    $map_scrollwhell = 'false'; 
  } else { 
    $map_scrollwhell = 'true'; 
  }

  if ( get_theme_mod( 'map_bw', customizer_library_get_default( 'map_bw' ) ) ) {
    $map_bw = -100;
  } else {
    $map_bw = 0;
  }

  echo '
  <!-- Google map -->
  <script type="text/javascript">
    function initMap() {
      var myLatlng = new google.maps.LatLng(' . $map_lat . ', ' . $map_lng .');
      var map_options = {
        zoom: ' . $map_zoom . ',
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: myLatlng,
        scrollwheel: ' . $map_scrollwhell . ',
        disableDefaultUI: true
      }

      var map = new google.maps.Map( document.getElementById( "map_canvas" ), map_options );
      var myIcon = new google.maps.MarkerImage("' . get_template_directory_uri() . '/images/map_icon.png", null, null, null, new google.maps.Size(34,45));
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        icon: myIcon
      });

      var styles = [{
        featureType: "all",
        stylers: [
          { saturation: ' . $map_bw . ' }
        ]
      }];

      map.setOptions({styles: styles});
    }
  </script>';
}

function mage_event_if_home() {
  /* if ( is_front_page() ) { 
    add_action( 'wp_head', 'mage_event_gmap' ); 
  } */
  if ( is_page_template( array ( 'onepage_var1.php', 'onepage.php' ) ) ) { 
    add_action( 'wp_footer', 'mage_event_gmap' ); 
  }
}

add_action( 'wp', 'mage_event_if_home' );

if ( function_exists( 'register_field_group' ) ) {
  require_once get_template_directory() . '/field_groups.php';
}

if ( file_exists( get_template_directory() . '/inc/customizer-library/customizer-library.php' ) ) {

  // helper library for the theme customizer
  require get_template_directory() . '/inc/customizer-library/customizer-library.php';
  // define options for the theme customizer
  require get_template_directory() . '/inc/customizer-options.php';
  // output inline styles based on theme customizer selections
  require get_template_directory() . '/inc/styles.php';
}

// define ACF mode
define( 'ACF_LITE', true );