<?php get_header(); ?>

<!-- Single Post Page (single.php, which loads blog-post.php) -->
<div class="single-page">
    <div class="container">

      <div class="content-page">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'blog', 'post' ); ?>

      <?php endwhile; else: ?>

        <p><?php _e( 'There are no posts or pages here', 'mage-event' ); ?></p>

      <?php endif; ?>

      <?php posts_nav_link(); ?>

      </div>

      <div class="sidebar">

        <div class="widgets">
          <?php if ( is_active_sidebar( 'mage_event_sidebar_widget_id' ) ) { ?>

            <?php dynamic_sidebar( 'mage_event_sidebar_widget_id' ); ?>

          <?php } ?>
        </div>

      </div>

  </div>
</div>
<!-- Single Post Page Ends! -->

<?php get_footer(); ?>