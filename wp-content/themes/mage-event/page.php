<?php get_header(); ?>

<!-- Single Page (page.php, which loads content-post.php) -->
<div class="single-page">
    <div class="container">

      <div class="content-page">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();

          get_template_part( 'content', 'page' );

          // If comments are open or we have at least one comment, load up the comment template.
          if ( comments_open() || get_comments_number() ) : comments_template(); endif;

        endwhile;
        ?>
      </div>

      <div class="sidebar">

        <div class="widgets">
        <?php if ( is_active_sidebar( 'mage_event_sidebar_widget_id' ) ) { ?>

          <?php dynamic_sidebar( 'mage_event_sidebar_widget_id' ); ?>

        <?php } ?>
        </div>
        
      </div>

  </div>
</div>
<!-- Single Page Ends! -->

<?php get_footer(); ?>
