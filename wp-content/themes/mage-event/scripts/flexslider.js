jQuery(document).ready(function($) {

  // FlexSlider
  $('.testimonials').flexslider({
      animation: 'slide',
      selector: '.slides blockquote',
      controlNav: false,
      directionNav: true,
      slideshowSpeed: 3600,
      animationSpeed: 1200,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>'
  });

  $('.sponsors .container .slides').flexslider({
      animation: 'slide',
      selector: 'ul li',
      controlNav: false,
      directionNav: true,
      itemWidth: 311,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>'
  });

});