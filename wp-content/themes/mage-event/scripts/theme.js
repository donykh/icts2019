jQuery(document).ready(function($) {

  // Single Speakers
  $('.event-speakers .speaker').hover(function() {
      $(this).find('div').slideToggle(150);
  });

  // Back to Top
  $('.back-to-top a').click(function(event) {
      $("html,body").animate({
          scrollTop: 0
      }, 2000);
      event.preventDefault();
  });

  // Contact Form
  $('.open-contact-form, a[rel=open-contact-form]').click(function(e) {
      $('.overlay').fadeIn('fast');
      e.preventDefault();
  });

  $('.close-contact-form').click(function(e) {
      $('.overlay').hide();
      e.preventDefault();
  });

  // Menu Scroll
  $('ul.menu li a').click(function(event) {
      $('html, body').animate({
          scrollTop: $($(this).attr('href')).offset().top
      }, 2000);
      event.preventDefault();
  });

  // Program Price select
  var $pricebox = $('.price div');

  $('.registration #select-price').prop("disabled", true);

  $pricebox.click(function(event) {
      $pricebox.removeClass('active');
      $(this).addClass('active');
      var $val = $(this).find('h4').text();
      $val = $.trim($val);
      $('.registration input#select-price').val($val);

  });

  // Register Scroll
  $('.register-now a').click(function(event) {
      $('html, body').animate({
          scrollTop: $($(this).attr('href')).offset().top
      }, 3000);
      event.preventDefault();
  });

  // Parallax effect
  $('.header').parallax("50%", 0.1);
  $('.why').parallax("50%", 0.1);
  $('.location').parallax("50%", 0.1);

  // Schedule
  $('.event-info p:not(.event-speaker)').hide();
  $('.event.extend span').html('<i class="fa fa-angle-down"></i>');

  // Extend on click
  $('.event.extend span').click(function(e) {

      var $span = $(this);
      var $event = $span.parent().parent();

      if ($span.html() == '<i class="fa fa-angle-up"></i>') {
          $span.html('<i class="fa fa-angle-down"></i>');
      } else {
          $span.html('<i class="fa fa-angle-up"></i>');
      };

      $event.find('.event-info p:not(.event-speaker)').toggle();

  });

  $('.form form').append('<input class="input-ticket" type="hidden" name="ticket" value=""></input>');

  $('body').on('click', '.price > div', function(e) {
      var selectedPrice = $(this).data('ticket-name');
      $('.input-ticket').val(selectedPrice);
  });

  /*
  // InView
  var $fadeInDown = $('div.menu, .header h1, .header .subtitle, .event-topics h3, .event-topics div i, .event-speakers .speaker h3');
  var $fadeInLeft = $('.when, .where, .event-speakers h2, .event-speakers .featured h3, .schedule h2, .bullets h3, .registration h2, .registration .form, .sponsors h2, .location h2, .maps .images, .maps #map_canvas, .social h2');
  var $fadeInRight = $('.register-now, .event-speakers .subtitle, .schedule .subtitle, .registration .subtitle, .registration .price, .sponsors .subtitle, .location .subtitle, .location .address, .social .subtitle');

  $fadeInDown.css('opacity', 0);
  $fadeInLeft.css('opacity', 0);
  $fadeInRight.css('opacity', 0);

  // InView - fadeInDown
  $fadeInDown.one('inview', function(event, visible) {
    if (visible) { $(this).addClass('animated fadeInDown'); }
  });

  // InView - fadeInLeft
  $fadeInLeft.one('inview', function(event, visible) {
    if (visible) { $(this).addClass('animated fadeInLeft'); }
  });

  // InView - fadeInRight
  $fadeInRight.one('inview', function(event, visible) {
    if (visible) { $(this).addClass('animated fadeInRight'); }
  });
  */

});