<?php

  register_field_group( array(
    'id' => 'acf_events',
    'title' => 'Events',
    'fields' => array(
      array(
        'key' => 'field_54c927dbab45e',
        'label' => 'Speaker',
        'name' => 'speaker',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c92843ab45f',
        'label' => 'Position',
        'name' => 'position',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54ca2a83ea923',
        'label' => 'Date',
        'name' => 'date',
        'type' => 'date_picker',
        'required' => 1,
        'date_format' => 'yymmdd',
        'display_format' => 'dd/mm/yy',
        'first_day' => 1,
      ),
      array(
        'key' => 'field_54c9254f36b8d',
        'label' => 'Starts at',
        'name' => 'starts_at',
        'type' => 'date_time_picker',
        'instructions' => 'When to events begin',
        'required' => 1,
        'show_date' => 'false',
        'date_format' => 'm/d/y',
        'time_format' => 'h:mm tt',
        'show_week_number' => 'false',
        'picker' => 'slider',
        'save_as_timestamp' => 'true',
        'get_as_timestamp' => 'false',
      ),
      array(
        'key' => 'field_54c93c7283a98',
        'label' => 'Ends at',
        'name' => 'ends_at',
        'type' => 'date_time_picker',
        'show_date' => 'false',
        'date_format' => 'm/d/y',
        'time_format' => 'h:mm tt',
        'show_week_number' => 'false',
        'picker' => 'slider',
        'save_as_timestamp' => 'true',
        'get_as_timestamp' => 'false',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'events',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'excerpt',
        2 => 'custom_fields',
        3 => 'discussion',
        4 => 'comments',
        5 => 'revisions',
        6 => 'slug',
        7 => 'author',
        8 => 'format',
        9 => 'featured_image',
        10 => 'categories',
        11 => 'tags',
        12 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
    register_field_group(array(
    'id' => 'acf_locations',
    'title' => 'Locations',
    'fields' => array(
      array(
        'key' => 'field_54c795504033f',
        'label' => 'Full address',
        'name' => 'full_address',
        'type' => 'text',
        'required' => 1,
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c7973687351',
        'label' => 'Category',
        'name' => 'category',
        'type' => 'select',
        'required' => 1,
        'choices' => array(
          'Nearby Accommodation' => 'Nearby Accommodation',
          'Venue' => 'Venue',
          'Conference Location' => 'Conference Location',
        ),
        'default_value' => 'Venue',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array(
        'key' => 'field_54c795e0c7375',
        'label' => 'Online booking available ',
        'name' => 'online_booking_available',
        'type' => 'checkbox',
        'choices' => array(
          'Yes' => 'Yes',
        ),
        'default_value' => '',
        'layout' => 'vertical',
      ),
      array(
        'key' => 'field_54c7961ec7376',
        'label' => 'Booking URL',
        'name' => 'booking_url',
        'type' => 'text',
        'required' => 1,
        'conditional_logic' => array(
          'status' => 1,
          'rules' => array(
            array(
              'field' => 'field_54c795e0c7375',
              'operator' => '==',
              'value' => 'Yes',
            ),
          ),
          'allorany' => 'all',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'locations',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'excerpt',
        2 => 'custom_fields',
        3 => 'discussion',
        4 => 'comments',
        5 => 'revisions',
        6 => 'slug',
        7 => 'author',
        8 => 'format',
        9 => 'featured_image',
        10 => 'categories',
        11 => 'tags',
        12 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
    register_field_group(array(
    'id' => 'acf_partners',
    'title' => 'Partners',
    'fields' => array(
      array(
        'key' => 'field_54c7e5b851297',
        'label' => 'Image',
        'name' => 'image',
        'type' => 'image',
        'required' => 1,
        'save_format' => 'url',
        'preview_size' => 'partners',
        'library' => 'all',
      ),
      array(
        'key' => 'field_54c7ee80d18e9',
        'label' => 'URL',
        'name' => 'url',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'partners',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'the_content',
        2 => 'excerpt',
        3 => 'custom_fields',
        4 => 'discussion',
        5 => 'comments',
        6 => 'revisions',
        7 => 'slug',
        8 => 'author',
        9 => 'format',
        10 => 'featured_image',
        11 => 'categories',
        12 => 'tags',
        13 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
    register_field_group(array(
    'id' => 'acf_speakers',
    'title' => 'Speakers',
    'fields' => array(
      array(
        'key' => 'field_54c25b30d5d70',
        'label' => 'First name',
        'name' => 'firstname',
        'type' => 'text',
        'required' => 1,
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c25c05e7ffe',
        'label' => 'Middle name',
        'name' => 'middle_name',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c25c24e7fff',
        'label' => 'Last name ',
        'name' => 'lastname',
        'type' => 'text',
        'required' => 1,
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c3c5cef7a48',
        'label' => 'Position',
        'name' => 'position',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => 'Position of person in company, organization etc.',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c3c637f7a4a',
        'label' => 'Twitter Profile',
        'name' => 'twitter_profile',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => 'Twitter profile URL',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c3c70e4790f',
        'label' => 'Featured Speaker',
        'name' => 'featured',
        'type' => 'select',
        'choices' => array(
          'Yes' => 'Yes',
          'No' => 'No',
        ),
        'default_value' => 'No',
        'allow_null' => 0,
        'multiple' => 0,
      ),
      array(
        'key' => 'field_54c3cbc6fb417',
        'label' => 'Profile image',
        'name' => 'profile_image',
        'type' => 'image',
        'required' => 1,
        'save_format' => 'url',
        'preview_size' => 'speaker',
        'library' => 'all',
      ),
      array(
        'key' => 'field_54c3cc70cd60c',
        'label' => 'Featured Image',
        'name' => 'featured_image',
        'type' => 'image',
        'conditional_logic' => array(
          'status' => 1,
          'rules' => array(
            array(
              'field' => 'field_54c3c70e4790f',
              'operator' => '==',
              'value' => 'Yes',
            ),
          ),
          'allorany' => 'all',
        ),
        'save_format' => 'url',
        'preview_size' => 'featured speaker',
        'library' => 'all',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'speakers',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'custom_fields',
        2 => 'discussion',
        3 => 'comments',
        4 => 'revisions',
        5 => 'author',
        6 => 'format',
        7 => 'categories',
        8 => 'tags',
        9 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
    register_field_group(array(
    'id' => 'acf_testimonials',
    'title' => 'Testimonials',
    'fields' => array(
      array(
        'key' => 'field_54c5744fe9473',
        'label' => 'Name',
        'name' => 'name',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c5746de9474',
        'label' => 'Position',
        'name' => 'position',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_format',
          'operator' => '==',
          'value' => 'quote',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
      ),
    ),
    'menu_order' => 0,
  ));
    register_field_group(array(
    'id' => 'acf_tickets',
    'title' => 'Tickets',
    'fields' => array(
      array(
        'key' => 'field_54c6a204c849e',
        'label' => 'Price',
        'name' => 'price',
        'type' => 'text',
        'required' => 1,
        'default_value' => '',
        'placeholder' => 'Price of the ticket',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c6a2a0c849f',
        'label' => 'Discounted',
        'name' => 'discounted',
        'type' => 'checkbox',
        'instructions' => 'Check if this is discounted option',
        'choices' => array(
          'Yes' => 'Yes',
        ),
        'default_value' => '',
        'layout' => 'vertical',
      ),
      array(
        'key' => 'field_54c6a3587eba9',
        'label' => 'Discounted label',
        'name' => 'discounted_label',
        'type' => 'text',
        'conditional_logic' => array(
          'status' => 1,
          'rules' => array(
            array(
              'field' => 'field_54c6a2a0c849f',
              'operator' => '==',
              'value' => 'Yes',
            ),
          ),
          'allorany' => 'all',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'tickets',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'excerpt',
        2 => 'custom_fields',
        3 => 'discussion',
        4 => 'comments',
        5 => 'revisions',
        6 => 'slug',
        7 => 'author',
        8 => 'format',
        9 => 'featured_image',
        10 => 'categories',
        11 => 'tags',
        12 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
    register_field_group(array(
    'id' => 'acf_topics',
    'title' => 'Topics',
    'fields' => array(
      array(
        'key' => 'field_54c51f6c5ca66',
        'label' => 'Font Awesome Icon',
        'name' => 'icon',
        'type' => 'text',
        'instructions' => 'Look for perfect icon at http://fortawesome.github.io/Font-Awesome/icons/ and input class name in the field, for example fa-check',
        'default_value' => 'fa-check',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_54c5219aa545c',
        'label' => 'Featured',
        'name' => 'featured',
        'type' => 'select',
        'instructions' => 'Select \'Yes\' if you want to promote this topic at the top of one page template under the Intro section',
        'choices' => array(
          'Yes' => 'Yes',
          'No' => 'No',
        ),
        'default_value' => 'No',
        'allow_null' => 0,
        'multiple' => 0,
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'topics',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array(
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'excerpt',
        2 => 'custom_fields',
        3 => 'discussion',
        4 => 'comments',
        5 => 'revisions',
        6 => 'author',
        7 => 'format',
        8 => 'featured_image',
        9 => 'categories',
        10 => 'tags',
        11 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
register_field_group(array (
	'id' => 'acf_subtitle',
	'title' => 'Subtitle',
	'fields' => array (
		array (
			'key' => 'field_56a4c97f0903e',
			'label' => 'Subtitle',
			'name' => 'subtitle',
			'type' => 'text',
			'default_value' => '',
			'placeholder' => 'Subtitle',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
	),
	'options' => array (
		'position' => 'acf_after_title',
		'layout' => 'no_box',
		'hide_on_screen' => array (
		),
	),
	'menu_order' => 0,
));
