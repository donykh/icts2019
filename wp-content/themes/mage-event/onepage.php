<?php

/**

 * @package WordPress

 * @subpackage Event



 Template Name: Homepage



*/

get_header(); ?>



  <?php get_template_part( 'inc/topics', 'onepage' ); //load onepage templ. part topics-onepage.php ?>



  <?php get_template_part( 'inc/speakers', 'onepage' ); //load onepage templ. part speakers-onepage.php ?>



  <?php get_template_part( 'inc/schedule', 'onepage' ); //load onepage templ. part schedule-onepage.php ?>



  <!-- why -->

  <div class="why">

    <div class="container">

    

    <?php get_template_part( 'inc/testimonials', 'onepage' ); //load onepage templ. part testimonials-onepage.php ?>



    <?php get_template_part( 'inc/bullets', 'onepage' ); //load onepage templ. part bullets-onepage.php ?>



    </div>

  </div>

  <!-- why ends! -->



  <?php get_template_part( 'inc/registration', 'onepage' ); //load onepage templ. part registration-onepage.php ?>



  <?php get_template_part( 'inc/sponsors', 'onepage' ); //load onepage templ. part sponsors-onepage.php ?>



  <?php get_template_part( 'inc/location', 'onepage' ); //load onepage templ. part location-onepage.php ?>



  <?php get_template_part( 'inc/social', 'onepage' ); //load onepage templ. part social-onepage.php ?>



  <!-- back to top -->

  <div class="back-to-top">

    <div class="container">



      <a href="#top"><i class="fa fa-angle-up fa-3x"></i></a>



    </div>

  </div>

  <!-- back to top ends! -->



  <?php

  $popup_form_shortcode = get_theme_mod( 'popup_form_shortcode', customizer_library_get_default( 'popup_form_shortcode' ) );

  if ( !empty( $popup_form_shortcode ) ) get_template_part( 'inc/form', 'onepage' );

  ?>



<?php get_footer(); ?>